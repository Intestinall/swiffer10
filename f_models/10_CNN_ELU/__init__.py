from tensorflow.keras import Sequential
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D, Dropout, Activation
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.activations import softmax, elu


EPOCHS = 500


def model(*args):
    custom_elu = lambda x: elu(x, alpha=args[0])
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding="same", input_shape=(32, 32, 3)))
    model.add(Activation(custom_elu))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation(custom_elu))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))

    model.add(Conv2D(64, (3, 3), padding="same"))
    model.add(Activation(custom_elu))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation(custom_elu))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.5))

    model.add(Flatten())
    model.add(Dense(args[1]))
    model.add(Activation(custom_elu))
    model.add(Dropout(0.5))
    model.add(Dense(10))
    model.add(Activation(softmax))
    model.compile(
        loss=sparse_categorical_crossentropy,
        optimizer=RMSprop(learning_rate=1e-4, decay=1e-6),
        metrics=[sparse_categorical_accuracy, "accuracy"]
    )
    return model, EPOCHS
