from tensorflow.keras import Sequential, Model
from tensorflow.keras.layers import Dense, Flatten, Add, Input
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.activations import softmax


EPOCHS = 500


def model(*args):
    input_layer = Input((32, 32, 3))
    flatten_layer_output = Flatten()(input_layer)

    has_previous_output = False
    penultimate_output = None
    last_output = flatten_layer_output

    for i in range(args[0]):
        if has_previous_output:
            add_output = Add()([last_output, penultimate_output])
            penultimate_output = add_output
            last_output = Dense(args[1], activation=args[2])(add_output)
        else:
            penultimate_output = last_output
            last_output = Dense(args[3], activation=args[4])(last_output)
            has_previous_output = True

    last_output = Add()([last_output, penultimate_output])

    output_tensor = Dense(10, activation=softmax)(last_output)
    model = Model(input_layer, output_tensor)

    model.compile(
        loss=sparse_categorical_crossentropy,
        optimizer=Adam(lr=1e-4),
        metrics=[sparse_categorical_accuracy, 'accuracy']
    )
    return model, EPOCHS
