from tensorflow.keras import Sequential, Model
from tensorflow.keras.layers import Dense, Flatten, Add, Input, Conv2D, Activation, MaxPooling2D, Dropout
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.activations import softmax, elu


EPOCHS = 500


def model(*args):
    input_layer = Input((32, 32, 3))
    flatten_layer_output = Conv2D(32, (3, 3), padding="same", activation=elu)(
        input_layer)
    flatten_layer_output = Dropout(0.5)(flatten_layer_output)

    counter = 1
    penultimate_output = flatten_layer_output
    last_output = Conv2D(32, (3, 3), padding="same", activation=elu)(
        flatten_layer_output)
    last_output = Dropout(0.5)(last_output)

    for i in range(args[0]):
        if counter == args[1]:
            add_output = Add()([last_output, penultimate_output])
            penultimate_output = add_output
            last_output = Conv2D(32, (3, 3), padding="same", activation=elu)(
                add_output)
            last_output = Dropout(0.5)(last_output)
            counter = 1
        else:
            last_output = Conv2D(32, (3, 3), padding="same", activation=elu)(
                last_output)
            last_output = Dropout(0.5)(last_output)
            counter += 1

    last_output = Add()([last_output, penultimate_output])
    last_output = Flatten()(last_output)
    output_tensor = Dense(10, activation=softmax)(last_output)
    model = Model(input_layer, output_tensor)

    model.compile(
        loss=sparse_categorical_crossentropy,
        optimizer=Adam(lr=1e-4),
        metrics=[sparse_categorical_accuracy, 'accuracy']
    )
    return model, EPOCHS
