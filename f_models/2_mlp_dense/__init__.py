from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.activations import softmax


EPOCHS = 500


def model(*args):
    model = Sequential()
    model.add(Flatten())
    model.add(Dense(args[0], activation=args[1]))
    model.add(Dense(args[2], activation=args[3]))
    model.add(Dense(args[4], activation=args[5]))
    model.add(Dense(10, activation=softmax))
    model.compile(
        loss=sparse_categorical_crossentropy,
        optimizer=Adam(lr=1e-4),
        metrics=[sparse_categorical_accuracy, "accuracy"]
    )
    return model, EPOCHS
