from tensorflow.keras import Model
from tensorflow.keras.layers import Conv2D, UpSampling2D, Concatenate, MaxPool2D, Input
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.activations import softmax, selu


def down_block(x, filters):
    c = Conv2D(filters, (3, 3), padding="same", strides=1, activation=selu)(x)
    c = Conv2D(filters, (3, 3), padding="same", strides=1, activation=selu)(c)
    p = MaxPool2D((2, 2), (2, 2))(c)
    return c, p


def up_block(x, skip, filters):
    us = UpSampling2D((2, 2))(x)
    concat = Concatenate()([us, skip])
    c = Conv2D(filters, (3, 3), padding="same", strides=1, activation=selu)(concat)
    c = Conv2D(filters, (3, 3), padding="same", strides=1, activation=selu)(c)
    return c


def bottleneck(x, filters):
    c = Conv2D(filters, (3, 3), padding="same", strides=1, activation=selu)(x)
    c = Conv2D(filters, (3, 3), padding="same", strides=1, activation=selu)(c)
    return c


def model():
    f = [16, 32, 64, 128, 256]
    inputs = Input((32, 32, 3))

    c1, p1 = down_block(inputs, f[0])  # 128 -> 64
    c2, p2 = down_block(p1, f[1])  # 64 -> 32
    c3, p3 = down_block(p2, f[2])  # 32 -> 16
    c4, p4 = down_block(p3, f[3])  # 16->8

    bn = bottleneck(p4, f[4])

    u1 = up_block(bn, c4, f[3])  # 8 -> 16
    u2 = up_block(u1, c3, f[2])  # 16 -> 32
    u3 = up_block(u2, c2, f[1])  # 32 -> 64
    u4 = up_block(u3, c1, f[0])  # 64 -> 128

    outputs = Conv2D(1, (1, 1), padding="same", activation=softmax)(u4)
    model = Model(inputs, outputs)
    model.compile(
        optimizer=Adam(lr=1e-4),
        loss=sparse_categorical_crossentropy,
        metrics=[sparse_categorical_accuracy, "accuracy"]
    )
    return model, 500
