from tensorflow.keras import Sequential
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D, AlphaDropout, Activation
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.activations import softmax, selu


EPOCHS = 500


def model(*args):
    model = Sequential()
    model.add(Conv2D(args[2], (args[4], args[4]), padding="same", input_shape=(32, 32, 3)))
    model.add(Activation(selu))
    model.add(Conv2D(args[2], (args[4], args[4])))
    model.add(Activation(selu))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(AlphaDropout(args[0]))

    model.add(Conv2D(args[3], (args[4], args[4]), padding="same"))
    model.add(Activation(selu))
    model.add(Conv2D(args[3], (args[4], args[4])))
    model.add(Activation(selu))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(AlphaDropout(args[0]))

    model.add(Flatten())
    model.add(Dense(args[1], kernel_initializer="lecun_normal"))
    model.add(Activation(selu))
    model.add(AlphaDropout(args[0]))
    model.add(Dense(10, kernel_initializer="lecun_normal"))
    model.add(Activation(softmax))
    model.compile(
        loss=sparse_categorical_crossentropy,
        optimizer=RMSprop(learning_rate=1e-4, decay=1e-6),
        metrics=[sparse_categorical_accuracy, "accuracy"]
    )
    return model, args[5]
