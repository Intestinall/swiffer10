from tensorflow.keras import Sequential, Model
from tensorflow.keras.layers import Dense, Flatten, Add, Input
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.activations import softmax, relu


EPOCHS = 500


def model(*args):
    input_layer = Input((32, 32, 3))
    flatten_layer_output = Flatten()(input_layer)

    counter = 1
    penultimate_output = flatten_layer_output
    last_output = Dense(3072, activation=relu)(flatten_layer_output)

    for i in range(args[0]):
        if counter == args[1]:
            add_output = Add()([last_output, penultimate_output])
            penultimate_output = add_output
            last_output = Dense(3072, activation=relu)(add_output)
            counter = 1
        else:
            last_output = Dense(3072, activation=relu)(last_output)
            counter += 1

    last_output = Add()([last_output, penultimate_output])
    output_tensor = Dense(10, activation=softmax)(last_output)
    model = Model(input_layer, output_tensor)

    model.compile(
        loss=sparse_categorical_crossentropy,
        optimizer=Adam(lr=1e-4),
        metrics=[sparse_categorical_accuracy, 'accuracy']
    )
    return model, EPOCHS
