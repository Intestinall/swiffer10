from tensorflow.keras import Sequential
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D, Dropout, Activation
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.activations import softmax


EPOCHS = 500


def model(*args):
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding="same", input_shape=(32, 32, 3)))
    model.add(Activation(args[0]))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation(args[1]))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(args[2]))

    model.add(Conv2D(64, (3, 3), padding="same"))
    model.add(Activation(args[3]))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation(args[4]))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(args[5]))

    model.add(Flatten())
    model.add(Dense(args[6]))
    model.add(Activation(args[7]))
    model.add(Dropout(args[8]))
    model.add(Dense(10))
    model.add(Activation(softmax))
    model.compile(
        loss=sparse_categorical_crossentropy,
        optimizer=RMSprop(learning_rate=1e-4, decay=1e-6),
        metrics=[sparse_categorical_accuracy, "accuracy"]
    )
    return model, EPOCHS
