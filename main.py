import tensorflow as tf
from tensorflow_core.python.keras.utils import plot_model

from utils import ModelsManager, limit_gpu_memory_usage

from tensorflow.keras.datasets import cifar10
# https://keras.io/examples/cifar10_cnn/
# https://appliedmachinelearning.blog/2018/03/24/achieving-90-accuracy-in-object-recognition-task-on-cifar-10-dataset-with-keras-convolutional-neural-networks/
#limit_gpu_memory_usage(tf, gigabytes=6)


(x_train, y_train), (x_test, y_test) = cifar10.load_data()
# shape (50000, 32, 32, 3)
# 50_000 elements
# 32x32 images
# RBG images

for model_wrapper in ModelsManager("f_models"):
    model_wrapper.write_running_name()

    try:
        #plot_model(model_wrapper.model, "TEST.png")
        model_wrapper.model.fit(
            x_train,
            y_train,
            epochs=model_wrapper.epochs,
            validation_data=(x_test, y_test),
            batch_size=1024,
            shuffle=True,
            callbacks=[model_wrapper.callback]
        )
        model_wrapper.save()
    except Exception as e:
        with open(f"errors/{model_wrapper.model_id}", "w") as f:
            f.write(repr(e))
