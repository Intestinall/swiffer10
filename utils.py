import csv
import importlib
import os
import sys
import warnings
from pathlib import Path

import tensorflow as tf
from tensorflow.keras.activations import *
from tensorflow.keras.applications import *
from tensorflow.keras.backend import *
from tensorflow.keras.callbacks import *
from tensorflow.keras.constraints import *
from tensorflow.keras.estimator import *
from tensorflow.keras.experimental import *
from tensorflow.keras.initializers import *
from tensorflow.keras.layers import *
from tensorflow.keras.losses import *
from tensorflow.keras.metrics import *
from tensorflow.keras.mixed_precision import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.premade import *
from tensorflow.keras.preprocessing import *
from tensorflow.keras.regularizers import *


class ModelWrapper:
    def __init__(self, model_f, name, line_number, params):
        self.model, self.epochs = model_f(*params)
        self.name = name
        self.line_number = line_number
        self.model_id = f"{name}_{line_number}"
        self.callback = self._get_callback()

    def _get_callback(self):
        return tf.keras.callbacks.TensorBoard(
            log_dir=Path("logs", self.model_id),
            histogram_freq=1
        )

    def write_running_name(self):
        with open("running_model", "w") as f:
            f.write(self.model_id)

    def save(self):
        self.model.save(Path("models", f"{self.model_id}.h5"))


class ModelsManager:
    def __init__(self, models_location):
        self._models_location = models_location

    def __iter__(self):
        for model_name, model_f, config_path in self.get_base_models():
            for line_number, params in self.get_configs(config_path):
                yield ModelWrapper(model_f, model_name, line_number, params)

    def get_base_models(self):
        for model_name in sorted(os.listdir(self._models_location)):
            path = Path(self._models_location, model_name)
            if self.is_model(path):
                model_f = self.get_model_function(path)
                yield model_name, model_f, Path(path, "config.csv")

    def get_configs(self, config_path):
        with open(config_path) as config_csv:
            config = csv.reader(config_csv)
            lines = (
                (i, l) for i, l in enumerate(config, start=1)
                if l and not l[0].startswith("#")
            )
            for i, line in lines:
                yield i, [self.cast(x) for x in line]

    def get_model_function(self, path):
        return importlib.import_module(self.path_to_module(path)).model

    @staticmethod
    def is_model(path):
        return path.is_dir() and not str(path).endswith("__")

    @staticmethod
    def path_to_module(path):
        return str(path).replace("/", ".")

    @staticmethod
    def cast(x):
        def is_(x, callable_):
            try:
                callable_(x)
                return True
            except ValueError:
                return False

        import_ = lambda x: getattr(sys.modules[__name__], x)
        for callable_ in (int, float, import_):
            try:
                return callable_(x)
            except (AttributeError, ValueError):
                pass
        warnings.warn(f'The value "{x}" had not been converted. Is it normal ?')
        return x


def limit_gpu_memory_usage(tf_, gigabytes):
    for gpu in tf_.config.experimental.list_physical_devices("GPU"):
        tf_.config.experimental.set_virtual_device_configuration(
            gpu,
            [
                tf_.config.experimental.VirtualDeviceConfiguration(
                    memory_limit=1024 * gigabytes
                )
            ]
        )
